﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comunio.Backend.Entities;
using System.Collections.ObjectModel;
using Comunio.Backend.Real;

namespace Comunio.Backend.TestRunners.WPFClient {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        const int COMPUTER_MANAGER_ID = 1;
        int current_manager_id = 2;

        public MainWindow() {
            InitializeComponent();

            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using (repo as IDisposable) {
                SetManagerListContent();
                SetMyTeamTabContent();
                SetMarketTabContent();
            }
        }

        private void SetManagerListContent() {
            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using (repo as IDisposable) {
                var managers = repo.GetAllManagers();
                foreach (var manager in managers) {
                    if (manager.Id != 1) {
                        managers_list.Items.Add(manager);
                    }
                }

                picked_manager_label.Content = repo.GetManagerById(current_manager_id);
            }
        }

        private void SetMarketTabContent() {
            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using (repo as IDisposable) {
                var players = repo.GetPlayersByManagerId(COMPUTER_MANAGER_ID);
                marketGrid.Items.Clear();

                foreach (var player in players) {
                    marketGrid.Items.Add(new {
                        Id = player.Id,
                        Name = player.Name,
                        Number = player.Number,
                        RealTeam = player.RealTeam.Name,
                        PlayingPosition = player.PlayingPosition.Name,
                        InitValue = player.InitValue,
                        MarketValue = player.MarketValue,
                        EntryDate = player.EntryDate,
                        Status = player.Status.Name,
                    });
                }

                marketGrid.IsReadOnly = true;
            }
        }

        private void SetMyTeamTabContent() {
            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using (repo as IDisposable) {
                var manager = repo.GetManagerById(current_manager_id);
                var players = repo.GetPlayersByManagerId(current_manager_id);
                playersGrid.Items.Clear();

                foreach (var player in players) {
                    playersGrid.Items.Add(new {
                        Id = player.Id,
                        Name = player.Name,
                        Number = player.Number,
                        RealTeam = player.RealTeam.Name,
                        PlayingPosition = player.PlayingPosition.Name,
                        InitValue = player.InitValue,
                        MarketValue = player.MarketValue,
                        EntryDate = player.EntryDate,
                        Status = player.Status.Name,
                    });
                }
                credit_label.Content = manager.Credit;
                manager_label.Content = manager.Name;
                playersGrid.IsReadOnly = true;
            }
        }

        //Event Handlers

        private void managers_list_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var man = (Manager)managers_list.SelectedItem;

            picked_manager_label.Content = man.Name;
            current_manager_id = man.Id;
            SetMyTeamTabContent();
        }

        private void Sell_Player_Button_Click(object sender, RoutedEventArgs e) {
            var selectedPlayer = playersGrid.SelectedItem;

            if (selectedPlayer == null) {

                ShowPlayerNotSelectedMessage();

            } else {

                System.Type playerType = selectedPlayer.GetType();

                int playerId = GetPlayerIdFromGridAnonymousPlayerObject(selectedPlayer, playerType);
                string playerName = GetPlayerNameFromGridAnonymousPlayerObject(selectedPlayer, playerType);
                int playerMarketValue = GetPlayerMarketValueFromGridAnonymousPlayerObject(selectedPlayer, playerType);

                MessageBoxResult messageBoxResult = ShowSellingConfirmationMessage(playerName, playerMarketValue);

                if (messageBoxResult == MessageBoxResult.Yes) {
                    bool sellingResult = SellPlayerToComputerAndUpdatePlayerLists(playerId);
                    if (sellingResult) {
                        ShowSoldPlayerMessage();
                    }
                }
            }
            
        }

        private void Buy_Player_Button_Click(object sender, RoutedEventArgs e) {
            var selectedPlayer = marketGrid.SelectedItem;

            if (selectedPlayer == null) {
                ShowPlayerNotSelectedMessage();
            } else {
                System.Type playerType = selectedPlayer.GetType();

                int playerId = GetPlayerIdFromGridAnonymousPlayerObject(selectedPlayer, playerType);
                string playerName = GetPlayerNameFromGridAnonymousPlayerObject(selectedPlayer, playerType);
                int playerMarketValue = GetPlayerMarketValueFromGridAnonymousPlayerObject(selectedPlayer, playerType);

                MessageBoxResult messageBoxResult = ShowBuyingConfirmationMessage(playerName, playerMarketValue);

                if (messageBoxResult == MessageBoxResult.Yes) {
                    bool buyResult = BuyPlayerAndUpdatePlayerLists(playerId);
                    if (buyResult) {
                        ShowBoughtPlayerMessage();
                    }
                }
            }
        }

        //Aux methods

        private bool SellPlayerToComputerAndUpdatePlayerLists(int playerId) {
            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using(repo as IDisposable) {
                Manager computer = repo.GetManagerById(COMPUTER_MANAGER_ID);
                Manager currentManager = repo.GetManagerById(current_manager_id);
                Player player = repo.GetPlayerById(playerId);
                int newManagerCredit = currentManager.Credit + player.MarketValue;
                int numberOfPlayers = repo.GetPlayersByManagerId(current_manager_id).Count();
                int minNumberOfPlayers = currentManager.Team.MinNumberOfPlayers;

                if (numberOfPlayers <= minNumberOfPlayers) {
                    ShowInsufficientPlayersMessage();
                    return false;
                } else {

                    repo.UpdateManagerCreditByManagerId(current_manager_id, newManagerCredit);
                    repo.UpdatePlayerTeamByPlayerId(playerId, computer.TeamId);

                    ReCalculateAllPlayersMarketPrice();
                    SetMarketTabContent();
                    SetMyTeamTabContent();
                    return true;
                }
            }
        }

        private bool BuyPlayerAndUpdatePlayerLists(int playerId) {
            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using(repo as IDisposable) {
                Manager currentManager = repo.GetManagerById(current_manager_id);
                Player player = repo.GetPlayerById(playerId);
                int newManagerCredit = currentManager.Credit - player.MarketValue;
                int numberOfPlayers = repo.GetPlayersByManagerId(current_manager_id).Count();
                int maxNumberOfPlayers = currentManager.Team.MaxNumberOfPlayers;

                if (newManagerCredit < 0) {
                    ShowInsufficientCreditMessage();
                    return false;
                } else if (numberOfPlayers >= maxNumberOfPlayers) {
                    ShowTooMuchPlayersMessage();
                    return false;
                } else {
                    repo.UpdateManagerCreditByManagerId(current_manager_id, newManagerCredit);
                    repo.UpdatePlayerTeamByPlayerId(playerId, currentManager.TeamId);

                    ReCalculateAllPlayersMarketPrice();
                    SetMarketTabContent();
                    SetMyTeamTabContent();
                    return true;
                }
            }
        }

        private void ReCalculateAllPlayersMarketPrice() {
            ComunioRepository repo = new ComunioRepository(new EfDbContext());
            using(repo as IDisposable) {
                IEnumerable<Player> players = repo.GetAllPlayers();
                Random random = new Random();
                double negativeFactorProbability = 0.5;
                double maxVariance = 0.2;

                foreach (var player in players) {
                    double factor = random.NextDouble() * maxVariance;
                    double negativeOrPositive = random.NextDouble();

                    if (negativeOrPositive <= negativeFactorProbability) {
                        factor = -factor;
                    }

                    int newMarketValue = Convert.ToInt32(player.MarketValue * (1 + factor));
                    repo.UpdatePlayerMarketValueByPlayerId(player.Id, newMarketValue);
                }
            }
        }

        private static int GetPlayerIdFromGridAnonymousPlayerObject(object selectedPlayer, Type playerType) {
            return (int)playerType.GetProperty("Id").GetValue(selectedPlayer, null);
        }

        private static int GetPlayerMarketValueFromGridAnonymousPlayerObject(object selectedPlayer, Type playerType) {
            return (int)playerType.GetProperty("MarketValue").GetValue(selectedPlayer, null);
        }

        private static string GetPlayerNameFromGridAnonymousPlayerObject(object selectedPlayer, Type playerType) {
            return (string)playerType.GetProperty("Name").GetValue(selectedPlayer, null);
        }

        private static void ShowPlayerNotSelectedMessage() {
            System.Windows.MessageBox.Show("You have to select a player from the list", "Player selection", System.Windows.MessageBoxButton.OK);
        }

        private static void ShowSoldPlayerMessage() {
            System.Windows.MessageBox.Show("Player sold", "Player selling confirmation", System.Windows.MessageBoxButton.OK);
        }

        private static MessageBoxResult ShowSellingConfirmationMessage(string playerName, int playerMarketValue) {
            return System.Windows.MessageBox.Show($"Are you sure you want to sell {playerName} for {playerMarketValue} credits?", "Player selling confirmation", System.Windows.MessageBoxButton.YesNo);
        }

        private static MessageBoxResult ShowBuyingConfirmationMessage(string playerName, int playerMarketValue) {
            return System.Windows.MessageBox.Show($"Are you sure you want to buy {playerName} for {playerMarketValue} credits?", "Player buying confirmation", System.Windows.MessageBoxButton.YesNo);
        }

        private static void ShowBoughtPlayerMessage() {
            System.Windows.MessageBox.Show("Player bought", "Player buying confirmation", System.Windows.MessageBoxButton.OK);
        }

        private static void ShowInsufficientCreditMessage() {
            System.Windows.MessageBox.Show("You have not enough credits to buy this player", "Player buying error", System.Windows.MessageBoxButton.OK);
        }

        private static void ShowInsufficientPlayersMessage() {
            System.Windows.MessageBox.Show("You can't sell this player.\nYour team must have at least 5 players", "Player selling error", System.Windows.MessageBoxButton.OK);
        }

        private static void ShowTooMuchPlayersMessage() {
            System.Windows.MessageBox.Show("Your team must not have more than 10 players", "Player buying error", System.Windows.MessageBoxButton.OK);
        }
    }
}
