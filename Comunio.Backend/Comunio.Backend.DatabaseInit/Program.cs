﻿using Comunio.Backend.Real;
using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.DatabaseInit {
    class Program {
        static void Main(string[] args) {
            Database.SetInitializer(new EfDbContextInitializer());

            using (var context = new EfDbContext()) {
                context.RealTeams.ToList();
                context.PlayingPositions.ToList();
                context.Statuses.ToList();
                context.Teams.ToList();
                context.Managers.ToList();
                context.Players.ToList();
            }
        }
    }
}
