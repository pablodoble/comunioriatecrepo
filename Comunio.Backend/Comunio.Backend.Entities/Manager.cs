﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Entities {
    public class Manager {
        //Primary Key
        public int Id { get; set; }

        //Foreing Key
        public int TeamId { get; set; }

        //Properties
        public string Name { get; set; }
        public int Credit { get; set; }

        //Navigation Properties
        public Team Team { get; set; }

        public override string ToString() {
            return Name;
        }
    }
}
