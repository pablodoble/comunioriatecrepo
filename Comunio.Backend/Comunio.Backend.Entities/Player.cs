﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Entities {
    public class Player {
        //Primary Key
        public int Id { get; set; }

        //Foreing Key
        public int TeamId { get; set; }
        public int StatusId { get; set; }
        public int PlayingPositionId { get; set; }
        public int RealTeamId { get; set; }

        //Properties
        public string Name { get; set; }
        public int Number { get; set; }
        public int InitValue { get; set; }
        public int MarketValue { get; set; }
        public DateTime EntryDate { get; set; }

        //Navigation Properties
        public Team Team { get; set; }
        public RealTeam RealTeam { get; set; }
        public PlayingPosition PlayingPosition { get; set; }
        public Status Status { get; set; }
    }
}
