﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Entities {
    public class Team {
        //Primary Key
        public int Id { get; set; }

        //Properties
        public int MaxNumberOfPlayers { get; set; }
        public int MinNumberOfPlayers { get; set; }

        //Navigation Properties
        public Manager Manager { get; set; }
        public ICollection<Player> Players { get; set; }
    }
}
