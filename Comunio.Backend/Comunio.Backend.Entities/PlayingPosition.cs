﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Entities {
    public class PlayingPosition {
        //Primary Key
        public int Id { get; set; }

        //Properties
        public string Name { get; set; }

        //Navigation Properties
        public ICollection<Player> Players { get; set; }
    }
}
