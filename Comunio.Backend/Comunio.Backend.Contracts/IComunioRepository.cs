﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Contracts {
    public interface IComunioRepository {
        IEnumerable<Player> GetPlayersByManagerId(int managerId);
        IEnumerable<Player> GetAllPlayers();
        IEnumerable<Manager> GetAllManagers();
        Player GetPlayerById(int playerId);
        Manager GetManagerById(int managerId);
        void UpdatePlayerTeamByPlayerId(int playerId, int newTeamId);
        void UpdatePlayerMarketValueByPlayerId(int playerId, int newMarketValue);
        void UpdateManagerCreditByManagerId(int managerId, int newCredit);
    }
}
