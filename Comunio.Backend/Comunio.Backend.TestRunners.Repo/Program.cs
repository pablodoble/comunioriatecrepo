﻿using Comunio.Backend.Contracts;
using Comunio.Backend.Real;
using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.TestRunners.Repo {
    class Program {
        static void Main(string[] args) {
            IComunioRepository repo = new ComunioRepository(new EfDbContext());

            using (repo as IDisposable) {
                var allplayers = repo.GetAllPlayers();
                Console.WriteLine("All Players: ");
                foreach (var p in allplayers) {
                    Console.WriteLine(p.Name);
                }

                var players0 = repo.GetPlayersByManagerId(1); //takes Computers' players
                var players1 = repo.GetPlayersByManagerId(2);
                var players2 = repo.GetPlayersByManagerId(3);

                Console.WriteLine("Manager 0 players: ");
                foreach (var p in players0) {
                    Console.WriteLine(p.Name);
                }
                Console.WriteLine("Manager 1 players: ");
                foreach (var p in players1) {
                    Console.WriteLine(p.Name);
                }
                Console.WriteLine("Manager 2 players: ");
                foreach (var p in players2) {
                    Console.WriteLine($"{p.Name}, {p.PlayingPosition}, {p.Team}, {p.Status}");
                }

                Console.WriteLine("Updating player market value...");
                repo.UpdatePlayerMarketValueByPlayerId(1, 123456);

                Console.WriteLine("Updating player team...");
                repo.UpdatePlayerTeamByPlayerId(2, 2);

            }
        }
    }
}
