﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comunio.Backend.Real;
using Comunio.Backend.Entities;


namespace Comunio.Backend.Tools.DatabaseInit
{
    public class Class1
    {
        static void Main(string[] args) {
            Database.SetInitializer(new EfDbContextInitializer());

            using (var context = new EfDbContext()) {
                context.Players.ToList();
                context.Teams.ToList();
                context.Managers.ToList();
                context.RealTeams.ToList();
                context.PlayingPositions.ToList();
                context.Statuses.ToList();
            }
        }
    }
}
