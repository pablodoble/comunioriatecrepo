﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comunio.Backend.Contracts;
using Comunio.Backend.Entities;
using System.Data.Entity;

namespace Comunio.Backend.Real {
    public class ComunioRepository : IComunioRepository, IDisposable {
        private EfDbContext _context;

        public ComunioRepository(EfDbContext context) {
            _context = context;
        }

        public void Dispose() {
            _context.Dispose();
        }

        public IEnumerable<Player> GetAllPlayers() {
            return _context.Players
                .Include(p => p.PlayingPosition)
                .Include(p => p.RealTeam)
                .Include(p => p.Team)
                .Include(p => p.Status)
                .ToList();
        }


        public IEnumerable<Manager> GetAllManagers() {
            return _context.Managers
                .Include(m => m.Team)
                .ToList();
        }


        public Player GetPlayerById(int playerId) {
            return _context.Players
                .Include(p => p.PlayingPosition)
                .Include(p => p.RealTeam)
                .Include(p => p.Team)
                .Include(p => p.Status)
                .Where(p => p.Id == playerId)
                .Single();

        }

        public Manager GetManagerById(int managerId) {
            return _context.Managers
                .Include(m => m.Team)
                .Where(m => m.Id == managerId)
                .Single();
        }

        public IEnumerable<Player> GetPlayersByManagerId(int managerId) {
            return _context.Players
                .Include(p => p.PlayingPosition)
                .Include(p => p.RealTeam)
                .Include(p => p.Team)
                .Include(p => p.Status)
                .Where(p => p.Team.Manager.Id == managerId)
                .ToList();
        }

        public void UpdatePlayerTeamByPlayerId(int playerId, int teamId) {
            var player = _context.Players.SingleOrDefault(p => p.Id == playerId);
            if(player != null) {
                UpdatePlayerTeam(teamId, player);
            }
        }

        private void UpdatePlayerTeam(int teamId, Player player) {
            player.TeamId = teamId;
            _context.Entry(player).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void UpdatePlayerMarketValueByPlayerId(int playerId, int marketValue) {
            var player = _context.Players.SingleOrDefault(p => p.Id == playerId);
            if(player != null) {
                UpdatePlayerMarketValue(marketValue, player);
            }
        }

        private void UpdatePlayerMarketValue(int marketValue, Player player) {
            player.MarketValue = marketValue;
            _context.Entry(player).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void UpdateManagerCreditByManagerId(int managerId, int newCredit) {
            var manager = _context.Managers.SingleOrDefault(m => m.Id == managerId);
            if (manager != null) {
                UpdateManagerCredit(newCredit, manager);
            }
        }

        private void UpdateManagerCredit(int newCredit, Manager manager) {
            manager.Credit = newCredit;
            _context.Entry(manager).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
