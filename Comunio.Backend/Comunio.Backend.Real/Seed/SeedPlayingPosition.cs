﻿using Comunio.Backend.Real.SeedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.Seed {
    public class SeedPlayingPosition {
        public static void GenerateSeedDataForPlayingPositions(EfDbContext context) {
            foreach (var playingPosition in PlayingPositionSeedEntities.GetEntities()) {
                context.PlayingPositions.Add(playingPosition);
            }
        }
    }
}
