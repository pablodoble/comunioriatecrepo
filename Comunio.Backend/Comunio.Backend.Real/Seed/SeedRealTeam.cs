﻿using Comunio.Backend.Real.SeedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.Seed {
   public class SeedRealTeam {
        public static void GenerateSeedDataForRealTeams(EfDbContext context) {
            foreach (var realteams in RealTeamSeedEntities.GetEntities()) {
                context.RealTeams.Add(realteams);
            }
        }
    }
}
