﻿using Comunio.Backend.Real.SeedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.Seed {
    public class SeedPlayer {
        public static void GenerateSeedDataForPlayers(EfDbContext context) {
            foreach (var player in PlayerSeedEntities.GetEntities()) {
                context.Players.Add(player);
            }
        }
    }
}
