﻿using Comunio.Backend.Real.SeedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.Seed {
    public class SeedManager {
        public static void GenerateSeedDataForManagers(EfDbContext context) {
            foreach (var manager in ManagerSeedEntities.GetEntities()) {
                context.Managers.Add(manager);
            }
        }
    }
}
