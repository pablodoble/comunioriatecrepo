﻿using Comunio.Backend.Real.SeedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.Seed {
    public class SeedStatus {
        public static void GenerateSeedDataForStatuses(EfDbContext context) {
            foreach (var status in StatusSeedEntities.GetEntities()) {
                context.Statuses.Add(status);
            }
        }
    }
}
