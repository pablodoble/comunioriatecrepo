﻿using Comunio.Backend.Real.SeedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.Seed {
    public class SeedTeam {
        public static void GenerateSeedDataForTeams(EfDbContext context) {
            foreach (var team in TeamSeedEntities.GetEntities()) {
                context.Teams.Add(team);
            }
        }
    }
}
