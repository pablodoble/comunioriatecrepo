﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.SeedEntities {
    public class PlayerSeedEntities {
        private static List<Player> _players = new List<Player> {
            new Player {
                Name = "Lionel Messi",
                EntryDate = new DateTime(2016,1,2),
                Number = 10,
                InitValue = 200000,
                MarketValue = 200000,
                TeamId = 3,
                StatusId = 1,
                PlayingPositionId = 1,
                RealTeamId = 2
            },
            new Player {
                Name = "Neymar",
                EntryDate = new DateTime(2016,1,9),
                Number = 11,
                InitValue = 300000,
                MarketValue = 300000,
                TeamId = 3,
                StatusId = 1,
                PlayingPositionId = 1,
                RealTeamId = 2
            },
            new Player {
                Name = "Luis Suarez",
                EntryDate = new DateTime(2016,1,9),
                Number = 9,
                InitValue = 200000,
                MarketValue = 200000,
                TeamId = 3,
                StatusId = 1,
                PlayingPositionId = 1,
                RealTeamId = 2
            },
            new Player {
                Name = "Adan Turan",
                EntryDate = new DateTime(2016,1,9),
                Number = 7,
                InitValue = 100000,
                MarketValue = 100000,
                TeamId = 3,
                StatusId = 1,
                PlayingPositionId = 3,
                RealTeamId = 2
            },
            new Player {
                Name = "Gerard Pique",
                EntryDate = new DateTime(2016,1,9),
                Number = 3,
                InitValue = 200000,
                MarketValue = 200000,
                TeamId = 3,
                StatusId = 1,
                PlayingPositionId = 2,
                RealTeamId = 2
            },
            new Player {
                Name = "Andres Iniesta",
                EntryDate = new DateTime(2016,1,9),
                Number = 8,
                InitValue = 300000,
                MarketValue = 300000,
                TeamId = 1,
                StatusId = 1,
                PlayingPositionId = 3,
                RealTeamId = 2
            },
            new Player {
                Name = "Marc-Andre ter Stegen",
                EntryDate = new DateTime(2016,1,9),
                Number = 1,
                InitValue = 100000,
                MarketValue = 100000,
                TeamId = 1,
                StatusId = 1,
                PlayingPositionId = 4,
                RealTeamId = 2
            },
            new Player {
                Name = "Christiano Ronaldo",
                EntryDate = new DateTime(2016,1,2),
                Number = 7,
                InitValue = 100000,
                MarketValue = 100000,
                TeamId = 1,
                StatusId = 1,
                PlayingPositionId = 1,
                RealTeamId = 1
            },
            new Player {
                Name = "Karim Benzema",
                EntryDate = new DateTime(2016,1,9),
                Number = 9,
                InitValue = 100000,
                MarketValue = 100000,
                TeamId = 1,
                StatusId = 1,
                PlayingPositionId = 1,
                RealTeamId = 1
            },
            new Player {
                Name = "James Rodriguez",
                EntryDate = new DateTime(2016,1,10),
                Number = 10,
                InitValue = 200000,
                MarketValue = 200000,
                TeamId = 2,
                StatusId = 1,
                PlayingPositionId = 3,
                RealTeamId = 1
            },
            new Player {
                Name = "Gareth Bale",
                EntryDate = new DateTime(2016,1,10),
                Number = 11,
                InitValue = 300000,
                MarketValue = 300000,
                TeamId = 2,
                StatusId = 1,
                PlayingPositionId = 3,
                RealTeamId = 1
            },
            new Player {
                Name = "Sergio Ramos",
                EntryDate = new DateTime(2016,1,10),
                Number = 4,
                InitValue = 100000,
                MarketValue = 100000,
                TeamId = 2,
                StatusId = 1,
                PlayingPositionId = 2,
                RealTeamId = 1
            },
            new Player {
                Name = "Isco",
                EntryDate = new DateTime(2016,1,10),
                Number = 22,
                InitValue = 200000,
                MarketValue = 200000,
                TeamId = 2,
                StatusId = 1,
                PlayingPositionId = 3,
                RealTeamId = 1
            },
            new Player {
                Name = "Keylor Navas",
                EntryDate = new DateTime(2016,1,10),
                Number = 1,
                InitValue = 100000,
                MarketValue = 100000,
                TeamId = 2,
                StatusId = 1,
                PlayingPositionId = 4,
                RealTeamId = 1
            },
            new Player {
                Name = "Pepe",
                EntryDate = new DateTime(2016,1,10),
                Number = 3,
                InitValue = 200000,
                MarketValue = 200000,
                TeamId = 2,
                StatusId = 1,
                PlayingPositionId = 4,
                RealTeamId = 1
            },
        };

        public static ICollection<Player> GetEntities() {
            return _players;
        }
    }
}
