﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.SeedEntities {
    public class StatusSeedEntities {
        private static List<Status> _statuses = new List<Status> {
            new Status {
                Id = 1,
                Name = "Active"
            },
            new Status {
                Id = 2,
                Name = "Injured"
            },
        };

        public static IEnumerable<Status> GetEntities() {
            return _statuses;
        }
    }
}
