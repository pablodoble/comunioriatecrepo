﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.SeedEntities {
    public class TeamSeedEntities {
        private static List<Team> _teams = new List<Team> {
            new Team {
                Id = 1,
                MaxNumberOfPlayers = 50,
                MinNumberOfPlayers = 0,
            },
            new Team {
                Id = 2,
                MaxNumberOfPlayers = 10,
                MinNumberOfPlayers = 5,
            },
            new Team {
                Id = 3,
                MaxNumberOfPlayers = 10,
                MinNumberOfPlayers = 5,
            }
        };

        public static IEnumerable<Team> GetEntities() {
            return _teams;
        }
    }
}
