﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.SeedEntities {
    public class ManagerSeedEntities {
        private static List<Manager> _managers = new List<Manager> {
            new Manager {
                 Id = 1,
                 Name = "Computer",
                 Credit = 999999,
                 TeamId = 1,
             },
            new Manager {
                Id = 2,
                Name = "Pablo",
                Credit = 500000,
                TeamId = 2,
            },
             new Manager {
                Id = 3,
                Name = "Pepito",
                Credit = 500000,
                TeamId = 3,
            }
        };

        public static IEnumerable<Manager> GetEntities() {
            return _managers;
        }
    }
}
