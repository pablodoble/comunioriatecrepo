﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.SeedEntities {
    public class PlayingPositionSeedEntities {
        private static List<PlayingPosition> _playing_positions = new List<PlayingPosition> {
            new PlayingPosition {
                Id = 1,
                Name = "Forward",
            },
            new PlayingPosition {
                Id = 2,
                Name = "Defender",
            },
            new PlayingPosition {
                Id = 3,
                Name = "Midfielder",
            },
            new PlayingPosition {
                Id = 4,
                Name = "Goalkeeper",
            },
        };

        public static IEnumerable<PlayingPosition> GetEntities() {
            return _playing_positions;
        }
    }
}
