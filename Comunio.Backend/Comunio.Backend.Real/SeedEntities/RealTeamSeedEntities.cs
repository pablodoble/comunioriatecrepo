﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.SeedEntities {
    public class RealTeamSeedEntities {
        private static List<RealTeam> _real_teams = new List<RealTeam> {
            new RealTeam {
                Id = 1,
                Name = "Real Madrid CF"
            },
            new RealTeam {
                Id = 2,
                Name = "FC Barcelona"
            },
        };

        public static IEnumerable<RealTeam> GetEntities() {
            return _real_teams;
        }
    }
}
