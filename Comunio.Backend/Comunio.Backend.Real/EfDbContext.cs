﻿using Comunio.Backend.Entities;
using Comunio.Backend.Real.ModelConfigurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real {
    public class EfDbContext : DbContext {
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<RealTeam> RealTeams { get; set; }
        public DbSet<PlayingPosition> PlayingPositions { get; set; }
        public DbSet<Status> Statuses { get; set; }

        public EfDbContext() : base("COMUNIO") {
            Configuration.LazyLoadingEnabled = false;
        }

        public EfDbContext(string connectionString) : base(connectionString) {
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Configurations.Add(new ManagerConfiguration());
            modelBuilder.Configurations.Add(new PlayerConfiguration());
            modelBuilder.Configurations.Add(new PlayingPositionConfiguration());
            modelBuilder.Configurations.Add(new RealTeamConfiguration());
            modelBuilder.Configurations.Add(new StatusConfiguration());
            modelBuilder.Configurations.Add(new TeamConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
