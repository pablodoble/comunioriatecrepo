﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.ModelConfigurations {
    public class PlayerConfiguration : EntityTypeConfiguration<Player> {
        public PlayerConfiguration() {
            ToTable("Player");

            HasRequired(p => p.PlayingPosition)
                .WithMany(pp => pp.Players)
                .HasForeignKey(p => p.PlayingPositionId)
                .WillCascadeOnDelete(false);

            HasRequired(p => p.Status)
                .WithMany(s => s.Players)
                .HasForeignKey(p => p.StatusId)
                .WillCascadeOnDelete(false);

            HasRequired(p => p.RealTeam)
                .WithMany(rt => rt.Players)
                .HasForeignKey(p => p.RealTeamId)
                .WillCascadeOnDelete(false);

            HasRequired(p => p.Team)
                .WithMany(t => t.Players)
                .HasForeignKey(p => p.TeamId)
                .WillCascadeOnDelete(false);

            Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
