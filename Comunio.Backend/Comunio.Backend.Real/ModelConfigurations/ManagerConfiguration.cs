﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comunio.Backend.Entities;

namespace Comunio.Backend.Real.ModelConfigurations {
    public class ManagerConfiguration : EntityTypeConfiguration<Manager> {
        public ManagerConfiguration() {
            ToTable("Manager");

            HasRequired(m => m.Team)
                .WithRequiredDependent(t => t.Manager);
        }
    }
}
