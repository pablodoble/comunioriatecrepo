﻿using Comunio.Backend.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real.ModelConfigurations {
    public class TeamConfiguration : EntityTypeConfiguration<Team> {
        public TeamConfiguration() {
            ToTable("Team");
            
        }
    }
}
