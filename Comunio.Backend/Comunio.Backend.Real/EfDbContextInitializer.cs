﻿using Comunio.Backend.Real.Seed;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunio.Backend.Real {
    public class EfDbContextInitializer : DropCreateDatabaseAlways<EfDbContext> {
        protected override void Seed(EfDbContext context) {

            SeedManager.GenerateSeedDataForManagers(context);
            SeedPlayer.GenerateSeedDataForPlayers(context);
            SeedPlayingPosition.GenerateSeedDataForPlayingPositions(context);
            SeedRealTeam.GenerateSeedDataForRealTeams(context);
            SeedStatus.GenerateSeedDataForStatuses(context);
            SeedTeam.GenerateSeedDataForTeams(context);

            base.Seed(context);
        }
    }
}
